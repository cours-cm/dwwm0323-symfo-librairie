$livre = new Livre();
        $livre->setTitre("Prélude à la fondation");
        $livre->setSlug("prelude-a-la-fondation");
        $livre->setDescription("Hari Seldon venait d'inventer la psychohistoire et il n'y voyait qu'une pure spéculation, sans application pratique. La psychohistoire ne pouvait pas prédire l'avenir ? Les politiques s'en moquaient. Les gens allaient y croire. Ensuite, les équations diraient ce qu'on leur ferait dire. Et si Seldon n'était pas d'accord, tant pis pour lui !
Alors, le jeune chercheur s'enfuit. Traqué, il sillonna les dédales souterrains de la planète Trantor, capitale de l'Empire galactique. Et ce qu'il vit le stupéfia. Un avenir inquiétant se dessinait sous ses yeux. Était-il trop tard pour éviter la catastrophe ?");
        $livre->setIsActive(true);
        $livre->setPrix(8.60);
        $livre->setCategorie($this->getReference(CategorieFixtures::SCIENCE_FICTION));
        $manager->persist($livre);
        $manager->flush();


$livre = new Livre();
        $livre->setTitre("La hanse galactique: Tome 1 Le prince marchand");
        $livre->setSlug("la-hanse-galactique-tome-1-le-prince-marchand");
        $livre->setDescription("XXIIIe siècle. Nicholas Van Rijn dirige la Compagnie solaire des épices et liqueurs au sein de la Ligue polesotechnique, l'alliance des négociants interstellaires. La Ligue transcende toutes les frontières politiques, et son pouvoir est à la mesure de l'étendue des routes commerciales : pour le moins considérable.
Des princes-marchands parcourant l'univers, Van Rijn est probablement le plus flamboyant, truculent et ingénieux. Débonnaire et fin négociateur, il arpente les mondes, mettant sa verve et son panache au service de ses intérêts et de ceux de la Ligue.
Voici ses aventures...");
        $livre->setIsActive(true);
        $livre->setPrix(8.60);
        $livre->setCategorie($this->getReference(CategorieFixtures::SCIENCE_FICTION));
        $manager->persist($livre);
        $manager->flush();

$livre = new Livre();
        $livre->setTitre("Après nous les oiseaux");
        $livre->setSlug("apres-nous-les-oiseaux");
        $livre->setDescription("La nouvelle voix enchanteresse de la science-fiction danoise, Prix Michael Strunge 
Quelque chose est arrivé. Le monde est en ruine. Il ne reste qu’une survivante. Assoiffée de grand air et de large, elle doit s’aventurer hors de ses repères. Dans l’oubli hypnotique du monde d’avant, elle marche, sans s’arrêter, jusqu’à apercevoir la mer. Bientôt elle sent son identité lui échapper. La nature a repris ses droits. Comment vivre désormais ?");
        $livre->setIsActive(true);
        $livre->setPrix(18.00);
        $livre->setCategorie($this->getReference(CategorieFixtures::SCIENCE_FICTION));
        $manager->persist($livre);
        $manager->flush();