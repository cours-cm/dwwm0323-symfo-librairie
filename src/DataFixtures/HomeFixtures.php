<?php

namespace App\DataFixtures;

use App\Entity\Home;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class HomeFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $home = new Home();
        $home->setTitre('Bienvenue sur le site de la Librairie');
        $home->setTexte("Découvrez sur notre site une sélection des meilleurs livres du moment et des dernières parutions. Vous pouvez également retrouver nos coups de coeur et nos sélections de livres pour vous aider à faire votre choix.");
        $home->setIsActive(true);
        $manager->persist($home);

        $home = new Home();
        $home->setTitre('Bienvenue sur le site de la Librairie');
        $home->setTexte("C'est l'été ! Découvrez notre sélection de livres pour les vacances.");
        $home->setIsActive(false);
        $manager->persist($home);

        $manager->flush();
    }
}
